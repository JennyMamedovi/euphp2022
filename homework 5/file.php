<?php
include 'function.php';

$password = 'mypass'; 
$max_upload_size = 1024*1024*2; 

if ($_SERVER['REQUEST_METHOD'] === 'POST')
{
    if (!empty($_FILES['file']) and !empty($_POST['password']))
    {   
        
        if ($_POST['password'] === $password)
        {
            
            if ($_FILES['file']['size'] < $max_upload_size) {

                
                $path = 'uploads/' . uniqid() . '.';
                $path .= pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
                $name = $_FILES['file']['name'];

                
                if ( move_uploaded_file($_FILES['file']['tmp_name'], $path) )
                {
                    
                    mysql_query("INSERT INTO files (`name`, `path`)
                                VALUES('$name', '$path')");

                   
                    $message = 'File Uploaded!';

                } else {
                    $message = 'Upload fail';
                }
            } else {
                $message = 'File is too large';
            }
        } else {
            $message = 'Wrong Password!';
        }
    } else {
        $message = 'File not set';
    }
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>File download</title>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
    <div class="container">
        <h1>Upload File</h1>
        
        <form>
      <select name="folder">
        <option value="folder1">Folder 1</option>
        <option value="folder2">Folder 2</option>
        <option value="folder3">Folder 3</option>
      </select>
    </form>

        
        <?php if(isset($message)): ?>
            <p class="alert"><?php echo $message; ?></p>
        <?php endif ?>

        
        <form action="" method="post" enctype="multipart/form-data">
            <input type="file" name="file">
            <input type="password" name="password" placeholder="Password">
            <input type="submit" name="upload" value="Upload">
        </form>

    </div>
</body>
</html>
